#
# spec file for package git-verify
#
# Copyright (c) 2022 git-verify contributors
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#

# Generated by rust2rpm 15-15-2.17
%global __cargo_skip_build 0

%global crate git-verify

Name:           %{crate}
Version:        0.0.0~git.c8529ec
Release:        0
Summary:        git-verify signatures, permissions and branch protection rules
Group:          Development/Libraries/Rust

# TODO licenses from depended crates
License:        GPL-3.0-only OR GPL-2.0-only

URL:            https://gitlab.com/source-security/git-verify.git
Source:         %{crate}-%{version}.tar.xz
Source1:        vendor.tar.xz

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust-packaging
BuildRequires:  libnettle-devel
BuildRequires:  clang-devel
BuildRequires:  gpg2
BuildRequires:  git

%global _description %{expand:
%{summary} on the client
based on information stored natively in git.}

%description %{_description}

%prep
%setup -qa1
mkdir .cargo
#cargo_prep
# overwrites what pas done in cargo_prep
cat <<EOF >.cargo/config
[source.crates-io]
replace-with = "vendored-sources"

[source.vendored-sources]
directory = "vendor"
EOF

%build
%cargo_build

%install
%cargo_install
mkdir -p %{buildroot}/%{_bindir}
mv %{_builddir}/%{crate}-%{version}/.cargo/bin/git-verify %{buildroot}/%{_bindir}

%check
%cargo_test

%files
%doc README.md
%{_bindir}/git-verify

