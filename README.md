git-verify signatures, permissions and branch protection rules on the client
based on information stored natively in git.

Status: proof of concept, only use this software for testing!

```
$ git-verify
Good!
$ git log --graph # not everything seen here is implemented yet, see below
*   commit 000000b (HEAD -> master, origin/master, origin/HEAD)
|\  Signature: authorized for merge, review
| | Merge: 000000a 000001c
| | Author: Author A <a@example.com>
| |
| |     Merge request foo
| |
| |     Reviewed-by: Author A <a@example.com>
| |     Source-ref: refs/heads/foo https://other-git-hoster.example.com/author/fork
| |     Target-ref: refs/heads/master https://git.example.com/origin-repo
| |     Tested-sig: 111111111111111111111111111111111111111111111111111111111111111
| |     Tested-sig: 222222222222222222222222222222222222222222222222222222222222222
| |     Review-sig: 333333333333333333333333333333333333333333333333333333333333333
| |     Signed-off-by: Author A <a@example.com>
| |
| * commit 000001c
| | Signature: authorized for review
| | Author: Author B <b@example.com>
| |
| |     Looks good. Thank you, we really need this.
| |
| |     Reviewed-by: Author B <b@example.com>
| |     Signed-off-by: Author B <b@example.com>
| |
| * commit 000001b
| | Signature: verified
| | Author: Author C <c@example.com>
| |
| |     Message 1b
| |
| |     Signed-off-by: Author C <c@example.com>
| |
| * commit 000001a
|/  Signature: no signature
|   Author: Author D <d@example.com>
|
|       Message 1a
|
|       Signed-off-by: Author D <c@example.com>
|
*   commit 000000a
|\  Signature: authorized
| | Merge: 0000009 000002c
| | Author: Author A <a@example.com>
| |
| |     Merge request bar from https://git.example.com/repo into master.
[cut]
# to verify all commits in the current branch
git-verify HEAD
# to verify last 3 merges
git-verify HEAD~3..HEAD
```

Feature and todo list:

 - [ ] production quality
   - [ ] configuration syntax that can be kept stable
   - [ ] cli arguments that can be kept stable
   - [ ] automated tests
   - [ ] security review
 - finer control of what to verify
   - [x] argument of a single revision or two with two dots (see man gitrevisions)
   - [ ] tag itself instead of only its commit
   - [ ] all refs with --all
   - [ ] --merge-request
   - [ ] for checking if all other reviews are done: --config reviews=-1
 - [x] configuration commited in repo as .gittrust/config
   - [ ] config file inheritance with level commited in repo, local repo config, user and system
   - [ ] support config from submodule
 - [x] require Signed-off-by trailer
 - required reviews (aprovals) via Reviewed-by trailer
   - [x] option to require 1 review
   - [ ] support to require more than 1 reviews
   - [ ] ensure reviews by distinct people by mailmap
   - [ ] option to not allow self-review
   - [ ] option to only allow merge commits in branch
 - different authorizations
   - [ ] tag
     - [ ] use Proposed-tag trailer for reviewing in merge request?
   - [ ] review vs merge
   - [ ] tag based on regexes
   - [ ] branches based on regexes
   - [ ] different types of reviews (e.g. documentation, security)
   - [ ] file based (code owners)
 - verify commit signatures
   - [x] certificates (also known as public keys) in repo
   - [ ] read certificates from git objects in commits and index instead of only checkout/worktree
     - [ ] support symlinks
   - [ ] support submodules
     - [ ] whole .gittrust can be a submodule
     - [ ] one of the paths leads (possibly via symlink) into a subdirectory of a submodule
   - [x] from gpg in .gittrust/pgp-cert.asc.d/0x0000000000000000.asc (12 ascii chars from hex fingerprint)
   - [ ] from ssh
   - [ ] from x509
   - [ ] verify certificates are cross signed by enough others
   - [ ] support using as CI for self service adding your cert in a repo (hook on receiving push or bot merged pull request)
   - [ ] historical certs that only verify certain commits
 - [ ] historical configuration to be able to verify history with new config
 - repository integrity
   - [ ] verify Target-ref trailer in commits to protect branch names
     - [ ] hook to include it and Source-ref
   - [ ] verify tag name in tags
   - [ ] verify Target-repo in tags?
     - [ ] hook to include it?
   - [ ] checkpoint state of other refs in master as files added in commits?
   - [ ] verify default branch
   - [ ] verify all and optionally only the protected refs are present
   - [ ] support renaming protected branches
   - [ ] support redacting history via grafts
 - [ ] include out of band review or automated testing results (status checks)
   by listing its signature in a Review-sig or Tested-sig trailer in a merge
   commit via hook
   - the signed data for automated tests a.o. needs to include hashes for which
     versions of dependencies it was tested with to make it reproducible (see
     also in-toto/rebuilderd and Depends-On trailer)
   - [ ] maybe include additional information for the ease of human reader, like name and status description?
   - [ ] verify
   - [ ] option to localy run tests, publish and include signature
   - [ ] obtain Review-sig or Tested-sig signatures via
     - [ ] status api with argument of git-commit-id which gives a number of urls; define a way to get from human viewable url to data and signature
     - [ ] verify signatures from cache, so that the server has less latency when runnig git-verify during push
     - [ ] transparency log by git-commit-id,certificate which gives list of signatures; need another way to look up the data from the signature
     - [ ] url with argument of git-commit-id which return data and signatures
     - [ ] file added in merge commit, data also included
     - [ ] git-note, data also included
     - [ ] cargo-crev
 - [ ] verify reproducible merge so it can be done by not fully trusted git hoster or bot
   - [ ] support different merge strategies
   - [ ] way to precompute merge for merge request to ensure low latency if it needs to be checked during push of the merge
 - [ ] [hook that verifies on fetch](https://git-scm.com/docs/githooks#_reference_transaction)
 - [ ] [hook pre-receive](https://git-scm.com/docs/githooks#pre-receive) to verify on receiving server
 - [ ] hook that verifies already on clone
   - at installation add hooks in the [default template
     directory](https://git-scm.com/docs/git-init#_template_directory) as [it
     reads like that is also used for
     clone](https://git-scm.com/docs/git-clone#Documentation/git-clone.txt---templatelttemplate-directorygt)
   - when running `git-verify setup` check if all three the [current
     repo](https://git-scm.com/docs/git-config#Documentation/git-config.txt-corehooksPath),
     the system and user wide [set template
     directory](https://git-scm.com/docs/git-config#Documentation/git-config.txt-inittemplateDir)
     also have the hooks
   - [hook post-checkout](https://git-scm.com/docs/githooks#_post_checkout) if
     the [hook reference-transaction used during
     fetch](https://git-scm.com/docs/githooks#_reference_transaction) is not enough
 - [ ] [hook pre-push](https://git-scm.com/docs/githooks#_pre_push) to verify
 - [ ] [hook pre-commit](https://git-scm.com/docs/githooks#_pre_commit) to
   automatically sets up commit signing, by among others creating a key without
   user promt
   - [ ] automatic key rotation
 - [ ] hook to show Signature status in e.g. git log
 - [ ] command to import/merge certificate
 - [ ] ability to bridge other keys into a WoT like OpenPGPs
 - [ ] command to sync with settings/acls of popular git hosters
 - [ ] easy to use in popular CIs

Related projects:
 - https://kernel.org keyring repo (git-verify is compatible with it: `git clone https://git.kernel.org/pub/scm/docs/kernel/pgpkeys.git && cd pgpkeys && git verify --pgp keys`)
 - https://radicle.xyz uses did:key for commit signatures. Its identity
   document, delegates and signatures could be supported in git-verify. See
   https://app.radicle.xyz/nodes/seed.radicle.xyz/rad:z3trNYnLWS11cJWC6BbxDs5niGo82/tree/0002-identity.md
   at commit 1c402116983be19e754fb14aa7ce38145f0a4b09 .
 - https://github.com/gittuf/gittuf
 - https://reproducible-builds.org
 - https://github.com/in-toto/rebuilderd
