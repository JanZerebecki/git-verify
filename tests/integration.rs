use std::io;
use std::env;
use std::file;
use std::path::Path;
use tempdir::TempDir;
use cmd_lib::{init_builtin_logger, run_cmd, run_fun};

#[test]
fn test() -> Result<(), io::Error> {
    let tmp_dir_o = TempDir::new("test_git-verify")?;
    let tmp_dir = tmp_dir_o.path();
    let gnupghome_buf = tmp_dir.join(".gnupg");
    let GNUPGHOME = gnupghome_buf.as_path();
    env::set_var("GNUPGHOME", GNUPGHOME);
    let email = "test@testing.com";
    let name = "Sam Tester";
    // TODO could have been compiled in non-debug mode
    let verify_buf = std::env::current_dir()?.join("target/debug/git-verify");
    let verify_release_buf = std::env::current_dir()?.join("target/release/git-verify");
    let mut verify = verify_buf.as_path();
    if !verify.exists() {
        verify = verify_release_buf.as_path();
        assert_eq!(verify.exists(), true)
    }
    init_builtin_logger();

    run_cmd!(
        echo;
        cd $tmp_dir;
        mkdir -p $GNUPGHOME;
        chmod 0700 $GNUPGHOME;
        gpg --no-tty --batch --default-new-key-algo "rsa2048/cert,sign+rsa2048/encr" --quick-generate-key --passphrase "" $email default;

        // smoke test for gpg
        echo "test" | gpg --no-tty --clearsign --armor;
        gpg --list-keys;
    )?;
    let keyid = run_fun!(gpg --list-secret-keys --with-colons|grep "^sec"|head -1|cut -d : -f 5)?;
    run_cmd!(
        cd $tmp_dir;

        echo test key id is: $keyid;
        mkdir -p .gittrust/pgp-cert.asc.d/;
        gpg --no-tty --export --armor $email >$tmp_dir/.gittrust/pgp-cert.asc.d/"0x"$keyid.asc;
        cat $tmp_dir/.gittrust/pgp-cert.asc.d/"0x"$keyid.asc;

        git init;
        // TODO why does it not work without configuring the keyid?
        git config user.signingkey $keyid;
        git config commit.gpgsign true;
        git config tag.forceSignAnnotated true;
        git config user.name $name;
        git config user.email $email;

        git add .gittrust;

        touch README.md;
        git add README.md;
        git commit -s -m "Add README";
        touch hello.md;
        git add hello.md;
        git commit -s -m "Add hello";
        find;

        RUST_BACKTRACE=1 $verify;
    )?;

    env::remove_var("GNUPGHOME");
    tmp_dir_o.close()?;
    Ok(())
}
